﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge
{
    public Node start;
    public Node end;
    public int weight = 1;

    public Edge(Node start, Node end, int weight)
    {
        this.start = start;
        this.end = end;
        this.weight = weight;
    }
}
