﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public int x;
    public int z;
    public int height;

    public Node(int x, int z, int height)
    {
        this.x = x;
        this.z = z;
        this.height = height;
    }

    public List<Edge> edge = new List<Edge>();
}
