﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph  {

    public int width;
    public int height;
    public int i;

    public List<Node> currentPath = null;
    public List<Node> pathMove = new List<Node>();
    public List<Node> pathAttack = new List<Node>();

    public Graph(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    public Node[,] CreateGraph(int width, int height)
    {
        Node[,] nodes = new Node[width, height];

        for(int i=0; i<width; i++)
        {
            for(int j=0; j<height; j++)
            {
                int h = Random.Range(1, 4);
                Node n = new Node(i, j, h);
                nodes[i, j] = n;
            }
        }
        BoardEdge(nodes, width, height);
        return nodes;
    }

    public void Edge(Node start, Node end, int weight)
    {
        start.edge.Add(new Edge(start, end, weight));
    }

    public void BoardEdge(Node[,] nodes, int width, int height)
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if(i != 0)
                {
                    Edge(nodes[i, j], nodes[i - 1, j],1);
                }
                if(i != height - 1)
                {
                    Edge(nodes[i, j], nodes[i + 1, j],1);
                }
                if (j != 0)
                {
                    Edge(nodes[i, j], nodes[i, j - 1],1);
                }
                if(j != height - 1)
                {
                    Edge(nodes[i, j], nodes[i, j + 1],1);
                }
            }
        }
    }



    public void AllPossiblePath(Tile t, float moveDistance, float jumpDistance)
    {
        List<Node> visitedMove = new List<Node>();
        int counter = 0;
        pathMove = PossiblePath(BoardManager.instance.nodes[t.x, t.z], moveDistance, jumpDistance, counter, visitedMove, pathMove);
        ChangeMaterialMove(pathMove);       
    }

    public List<Node> PossiblePath(Node n, float moveDistance, float jumpDistance, int counter, List<Node> visitedMove, List<Node> pathMove)
    {
        if (counter > moveDistance)
        {
            return pathMove;
        }
        else
        {
            if (!pathMove.Contains(n) && !NodeIsOccupied(n))
            {
                pathMove.Add(n);
            }

            jumpDistance = jumpDistance + n.height;
            visitedMove.Add(n);
            for (int i = 0; i < n.edge.Count; i++)
            {
                bool isVisited = false;
                for (int j = 0; j < visitedMove.Count; j++)
                {
                    if (n.edge[i].end == visitedMove[j])
                    {
                        isVisited = true;
                    }
                }
                if (!isVisited && !NodeIsOccupied(n.edge[i].end))
                {
                    if (n.height == n.edge[i].end.height)
                    {
                        PossiblePath(n.edge[i].end, moveDistance, jumpDistance - n.height, counter + 1, visitedMove, pathMove);
                    }
                    else if (n.height > n.edge[i].end.height)
                    {
                        if ((2 * n.height) - jumpDistance <= n.edge[i].end.height)
                        {
                            PossiblePath(n.edge[i].end, moveDistance, jumpDistance - n.height, counter + 1, visitedMove, pathMove);
                        }
                    }
                    else if (n.height < n.edge[i].end.height)
                    {
                        if (jumpDistance >= n.edge[i].end.height)
                        {
                            PossiblePath(n.edge[i].end, moveDistance, jumpDistance - n.height, counter + 1, visitedMove, pathMove);
                        }
                    }
                }
            }
            visitedMove.Remove(n);
        }
        return pathMove;
    }




    #region PathFinding
    public List<Node> PlayerPathFinding(Tile start, Tile end)
    {
        currentPath = null;
        Dictionary<Node, float> dist = new Dictionary<Node, float>();
        Dictionary<Node, Node> prev = new Dictionary<Node, Node>();

        currentPath = new List<Node>();

        List<Node> unvisited = new List<Node>();

        Node source = BoardManager.instance.nodes[start.x, start.z];
        Node target = BoardManager.instance.nodes[end.x, end.z];

        dist[source] = 0;
        prev[source] = null;

        foreach (Node v in BoardManager.instance.nodes)
        {
            if (v != source)
            {
                dist[v] = Mathf.Infinity;
                prev[v] = null;
            }

            unvisited.Add(v);
        }

        while (unvisited.Count > 0)
        {
            Node u = null;

            foreach (Node possibleU in unvisited)
            {
                if (u == null || dist[possibleU] < dist[u])
                {
                    u = possibleU;
                }
            }

            if (u == target)
            {
                break;
            }
            unvisited.Remove(u);
            
            for (int i = 0; i < u.edge.Count; i++)
            {
                Node n = u.edge[i].end;
                float alt = dist[u] + u.edge[i].weight;
                if (alt < dist[u.edge[i].end] && !NodeIsOccupied(n))
                {
                    dist[u.edge[i].end] = alt;
                    prev[u.edge[i].end] = u;
                }
            }
        }

        if (prev[target] == null)
        {
            currentPath.Clear();
            return currentPath;
        }

        Node curr = target;

        while (curr != null)
        {
            currentPath.Add(curr);
            curr = prev[curr];
        }
        currentPath.Reverse();
        
        return currentPath;        
    }
    #endregion




    #region Attack
    public void AllPossiblePathAttack(Tile start, float attackDistance)
    {
        List<Node> visited = new List<Node>();
        int counter = 0;
        pathAttack = PossiblePathAttack(BoardManager.instance.nodes[start.x, start.z], attackDistance, counter, pathAttack, visited);
        ChangeMaterialAttack(pathAttack);       
    }

    public List<Node> PossiblePathAttack(Node n, float attackDistance, int counter, List<Node> pathAttack, List<Node> visited)
    {
        if (counter > attackDistance)
        {
            return pathAttack;
        }
        else
        {
            if (!pathAttack.Contains(n) && !AttackDifferentTeam(n))
            {
                pathAttack.Add(n);
            }
            visited.Add(n);
            for (int i = 0; i < n.edge.Count; i++)
            {
                bool isVisited = false;
                for (int j = 0; j < visited.Count; j++)
                {
                    if (n.edge[i].end == visited[j])
                    {
                        isVisited = true;
                    }
                }
                if (!isVisited && !AttackDifferentTeam(n.edge[i].end))
                {
                    PossiblePathAttack(n.edge[i].end, attackDistance, counter + 1, pathAttack, visited);
                }
            }
            visited.Remove(n);
        }
        return pathAttack;
    }
    #endregion




    public void ChangeMaterialMove(List<Node> n)
    {
        for (int i = 0; i < n.Count; i++)
        {
            BoardManager.instance.tiles[n[i].x, n[i].z].isSelectable = true;
            BoardManager.instance.tiles[n[i].x, n[i].z].isMove = true;
            BoardManager.instance.tiles[n[i].x, n[i].z].GetComponent<Renderer>().material.color = Color.blue;
        }
    }

    public void ChangeMaterialAttack(List<Node> n)
    {
        for (int i = 0; i < n.Count; i++)
        {
            BoardManager.instance.tiles[n[i].x, n[i].z].isSelectable = true;
            BoardManager.instance.tiles[n[i].x, n[i].z].isAttack = true;
            BoardManager.instance.tiles[n[i].x, n[i].z].GetComponent<Renderer>().material.color = Color.red;
        }
    }

    public void ClearList(List<Node> n)
    {
        for (int i = 0; i < n.Count; i++)
        {
            BoardManager.instance.tiles[n[i].x, n[i].z].isSelectable = false;
            BoardManager.instance.tiles[n[i].x, n[i].z].GetComponent<Renderer>().material.color = BoardManager.instance.tiles[n[i].x, n[i].z].StartMaterial;

            if (BoardManager.instance.tiles[n[i].x, n[i].z].isMove == true)
            {
                BoardManager.instance.tiles[n[i].x, n[i].z].isMove = false;
            }
            else if (BoardManager.instance.tiles[n[i].x, n[i].z].isAttack == true)
            {
                BoardManager.instance.tiles[n[i].x, n[i].z].isAttack = false;
            }
        }
    }

    public bool NodeIsOccupied(Node n)
    {
        if (BoardManager.instance.tiles[n.x, n.z].isOccupied)
        {
            return true;
        }
        return false;
    }

    public bool AttackDifferentTeam(Node n)
    {
        for (i = 0; i < TurnManager.instance.players.Count; i++)
            if (TurnManager.instance.players[i].X == n.x && TurnManager.instance.players[i].Z == n.z)
            {
                if (TurnManager.instance.players[i].team == TurnManager.instance.CurrentPlayer.team)
                {
                    return true;
                }
            }
        return false;
    }
}
