﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardCreator : MonoBehaviour {

    public void Start()
    {
        InstanceBoard(BoardManager.instance.tiles);
    }

    public void InstanceBoard(Tile[,] tiles)
    {
        for (int i = 0; i < BoardManager.instance.width; i++)
        {
            for (int j = 0; j < BoardManager.instance.height; j++)
            {
                if(tiles[i, j].height == 1)
                {
                    GameObject Tile = Instantiate(Resources.Load("PrefabTile/TileP") as GameObject);
                    Tile.transform.position = new Vector3(tiles[i, j].x, 0.50f, tiles[i, j].z - 0.5f);
                    Tile.AddComponent<Tile>();
                    Tile.GetComponent<Renderer>().material.color = Color.cyan;
                    Tile.GetComponent<Tile>().x = tiles[i, j].x;
                    Tile.GetComponent<Tile>().z = tiles[i, j].z;
                    Tile.GetComponent<Tile>().height = tiles[i, j].height;
                    tiles[i, j] = Tile.GetComponent<Tile>();
                }
                else if(tiles[i, j].height == 2)
                {
                    GameObject RockTile = Instantiate(Resources.Load("PrefabTile/RockTileP") as GameObject);
                    RockTile.transform.position = new Vector3(tiles[i, j].x, 1, tiles[i, j].z - 0.5f);
                    RockTile.AddComponent<Tile>();
                    RockTile.GetComponent<Renderer>().material.color = Color.gray;
                    RockTile.GetComponent<Tile>().x = tiles[i, j].x;
                    RockTile.GetComponent<Tile>().z = tiles[i, j].z;
                    RockTile.GetComponent<Tile>().height = tiles[i, j].height;
                    tiles[i, j] = RockTile.GetComponent<Tile>();
                }
                else
                {
                    GameObject MountTile = Instantiate(Resources.Load("PrefabTile/MountTileP") as GameObject);
                    MountTile.transform.position = new Vector3(tiles[i, j].x, 1.50f, tiles[i, j].z-0.5f);
                    MountTile.AddComponent<Tile>();
                    MountTile.GetComponent<Renderer>().material.color = Color.green;
                    MountTile.GetComponent<Tile>().x = tiles[i, j].x;
                    MountTile.GetComponent<Tile>().z = tiles[i, j].z;
                    MountTile.GetComponent<Tile>().height = tiles[i, j].height;
                    tiles[i, j] = MountTile.GetComponent<Tile>();
                }               
            }
        }
    }

}
