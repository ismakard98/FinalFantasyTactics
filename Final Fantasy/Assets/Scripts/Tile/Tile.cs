﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    public bool isSelectable;
    public bool isOccupied;
    public bool selected;
    public bool isMove;
    public bool isAttack;
    public float speed = 5f;
    Quaternion rotation;
    Quaternion startRotation;
    public Color StartMaterial;

    public int i;
    public int x;
    public int z;
    public int height;
    
    public Tile(int x, int z, int height)
    {
        this.x = x;
        this.z = z;
        this.height = height;
    }

    public void Start()
    {
        rotation = new Quaternion(0, 180, 0, 0);
        startRotation = new Quaternion(0, 0, 0, 0);
        StartMaterial = GetComponent<Renderer>().material.color;
    }

    public void OnMouseEnter()
    {
        if(BoardManager.instance.inPlayer.allPlayerSelected == false)
        {
            GetComponent<Renderer>().material.color = Color.yellow;
        }

        if (isSelectable)
        {
            GetComponent<Renderer>().material.color = Color.yellow;
            if (GameManager.instance.colorPathMove)
            {
                BoardManager.instance.graph.PlayerPathFinding(BoardManager.instance.tiles[TurnManager.instance.CurrentPlayer.X, TurnManager.instance.CurrentPlayer.Z], this);
                ColorMove(BoardManager.instance.graph.currentPath);
            }
        }

        if (GameManager.instance.PlayerAttack)
        {
            TurnManager.instance.CurrentPlayer.selectAttackX = x;
            TurnManager.instance.CurrentPlayer.selectAttackZ = z;
            TurnManager.instance.UpdateInfoAttack();
        }

        if (isOccupied)
        {
            GetComponent<Renderer>().material.color = StartMaterial;
        }

    }

    public void OnMouseDown()
    {
        if (isSelectable)
        {
            TurnManager.instance.CurrentPlayer.selectedX = x;
            TurnManager.instance.CurrentPlayer.selectedZ = z;
            GameManager.instance.PathFinding = true;
            GameManager.instance.colorPathMove = false;
        }

        if (!BoardManager.instance.inPlayer.allPlayerSelected && !isOccupied)
        {
            if (!isOccupied)
            {
                if (z >= 7)
                {
                    BoardManager.instance.inPlayer.SetPlayer(x, z, height, rotation);
                }

                if (z < 7)
                {
                    BoardManager.instance.inPlayer.SetPlayer(x, z, height, startRotation);
                }

                isOccupied = true;
                isSelectable = false;
            }
        }
    }

    public void OnMouseUp()
    {
        if (isSelectable)
        {

            if (GameManager.instance.PathFinding)
            {
                BoardManager.instance.graph.PlayerPathFinding(BoardManager.instance.tiles[TurnManager.instance.CurrentPlayer.X, TurnManager.instance.CurrentPlayer.Z], this);
                ColorMove(BoardManager.instance.graph.currentPath);
            }

            if (GameManager.instance.PathFinding)
            {
                TurnManager.instance.CurrentPlayer.playerMoving = true;
            }

            else if (GameManager.instance.PlayerAttack)
            {
                TurnManager.instance.CurrentPlayer.playerAttack = true;
            }

        }
    }

    public void OnMouseExit()
    {

        if (isOccupied)
        {
            GetComponent<Renderer>().material.color = StartMaterial;
        }

        if (isSelectable)
        {
            GetComponent<Renderer>().material.color = Color.yellow;
        }

        if (isMove)
        {
            GetComponent<Renderer>().material.color = Color.blue;
            if (GameManager.instance.colorPathMove)
            {
                ResetColor(BoardManager.instance.graph.currentPath);
            }
        }

        else if (isAttack)
        {
            GetComponent<Renderer>().material.color = Color.red;
        }

        else
        {
            GetComponent<Renderer>().material.color = StartMaterial;
        }

    }

    public void ColorMove(List<Node> n)
    {
        for(int i=0; i<n.Count; i++)
        {
            BoardManager.instance.tiles[n[i].x, n[i].z].GetComponent<Renderer>().material.color = Color.yellow;
        }
    }

    public void ResetColor(List<Node> n)
    {
        for (int i = 0; i < n.Count; i++)
        {
            if (isMove)
            {
                BoardManager.instance.tiles[n[i].x, n[i].z].GetComponent<Renderer>().material.color = Color.blue;
            }
        }
    }
}
