﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingSound : MonoBehaviour
{
    public GameObject audioSource;
    public int capacity;

    public List<GameObject> active = new List<GameObject>();

    public Queue<GameObject> inactive = new Queue<GameObject>();

    void Awake()
    {
        for (int i = 0; i < capacity; i++)
        {
            GameObject go = Instantiate(audioSource);
            go.transform.parent = transform;
            go.SetActive(false);

            GameObject audioChannel = go.GetComponent<GameObject>();
            inactive.Enqueue(audioChannel);
        }
       
    }

    public void Fetch()
    {
        if (inactive.Count > 0)
        {
            GameObject audioSource = inactive.Dequeue();
            active.Add(audioSource);
            audioSource.GetComponent<AudioSource>().clip = null;
            audioSource.gameObject.SetActive(true);
            for(int i = 0; i < capacity; i++)
            {
                if(active[i] == null)
                {
                    active[i] = audioSource;
                    break;
                }
            }
        }
    }

    public void Push(AudioClip clip)
    {
        for(int i = 0; i < capacity; i++)
        {
            if (active[i].gameObject.GetComponent<AudioSource>().clip == clip && active[i] != null)
            {
                active[i].gameObject.SetActive(false);
                inactive.Enqueue(active[i]);
                active[i] = null;
                break;
            }
        }
    }
}
