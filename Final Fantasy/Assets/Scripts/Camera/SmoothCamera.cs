﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmoothCamera : MonoBehaviour {

    public float speed = 8f;
    public float speedRot = 5f;
    public Transform pathParent;
    Transform targetPoint;
    int index;


    void OnDrawGizmos()
    {
        Vector3 from;
        Vector3 to;
        for (int a = 0; a < pathParent.childCount; a++)
        {
            from = pathParent.GetChild(a).position;
            to = pathParent.GetChild((a + 1) % pathParent.childCount).position;
            Gizmos.color = new Color(1, 0, 0);
            Gizmos.DrawLine(from, to);
        }
    }
    void Start()
    {
        index = 0;
        targetPoint = pathParent.GetChild(index);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            index++;
            index %= pathParent.childCount;
            targetPoint = pathParent.GetChild(index);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if(index == 0)
            {
                index = index + 3;
                index %= pathParent.childCount;
                targetPoint = pathParent.GetChild(index);
            }
            else if (index != 0)
            {
                index--;
                index %= pathParent.childCount;
                targetPoint = pathParent.GetChild(index);
            }
        }
        transform.position = Vector3.Lerp(transform.position, targetPoint.position, speed * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetPoint.rotation, speedRot * Time.deltaTime);
    }      
}