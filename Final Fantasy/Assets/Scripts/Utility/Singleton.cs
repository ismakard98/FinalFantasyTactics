﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T m_Instance = default(T);

    public static T instance
    {
        get
        {
            if (m_Instance == null)
            {
                T[] instances = GameObject.FindObjectsOfType<T>();

                if (instances.Length == 1)
                {
                    m_Instance = instances[0];
                    Debug.LogError("Singleton of type" + typeof(T).FullName + "was aleady there");
                }
                else
                {
                    for (int i = 0; i < instances.Length; i++)
                    {
                        Destroy(instances[i].gameObject);
                    }

                    GameObject go = new GameObject();
                    go.name = typeof(T).Name;
                    m_Instance = go.AddComponent<T>();
                }
            }
            return m_Instance;
        }
    }

}
