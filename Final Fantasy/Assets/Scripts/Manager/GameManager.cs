﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public Player player;

    public bool move;
    public bool attack;
    public bool endTurn;
    public bool AllPlayerPositioned;
    public bool PathFinding;
    public bool PlayerAttack;
    public bool endGame;
    public bool colorPathMove;

    private float interpolateDuration = 1f;
    private float mLerp = 0.5f;

    public bool create;

    private void Awake()
    {
        BoardManager.instance.create = true;
        TurnManager.instance.create = true;
    }

    private void Start()
    {
        if (!AllPlayerPositioned)
        {
            SetCurrentState(State.StartGame);
        }
    }

    public enum State
    {
        Idle,
        StartGame,
        MovePlayer,
        AttackPlayer,
        EndTurn
    }
    public State currentState;

    public void SetCurrentState(State state)
    {
        currentState = state;
    }

    void Update()
    {
        switch (currentState)
        {
            case State.Idle:

                move = true;
                endTurn = false;
                break;

            case State.StartGame:

                if (BoardManager.instance.inPlayer.allPlayerSelected)
                {
                    AllPlayerPositioned = true;
                }
                if (AllPlayerPositioned)
                {
                    TurnManager.instance.AddPlayerQueue();
                    SetCurrentState(State.Idle);
                }
                break;

            case State.MovePlayer:

                break;

            case State.AttackPlayer:

                attack = false;
                endTurn = true;

                colorPathMove = false;
                PathFinding = false;
                PlayerAttack = true;
                break;

            case State.EndTurn:

                endTurn = true;

                PlayerAttack = false;
                break;
        }
        if (AllPlayerPositioned)
        {
            ThisPlayer(BoardManager.instance.tiles[TurnManager.instance.CurrentPlayer.X, TurnManager.instance.CurrentPlayer.Z]);
            TurnManager.instance.CheckDeadPlayer();
            TurnManager.instance.Victory();
        }
        if (endGame)
        {
            move = false;
            attack = false;
            endTurn = false;
        }
    }

    public void ThisPlayer(Tile t)
    {
        if (TurnManager.instance.CurrentPlayer.team == 1)
        {
            mLerp = Mathf.PingPong(Time.time, interpolateDuration);
            t.GetComponent<Renderer>().material.color = Color.Lerp(Color.yellow, Color.blue, mLerp);
        }
        else
        {
            mLerp = Mathf.PingPong(Time.time, interpolateDuration);
            t.GetComponent<Renderer>().material.color = Color.Lerp(Color.yellow, Color.red, mLerp);
        }
    }

    void OnGUI()
    {
        if (move)
        {
            if (GUI.Button(new Rect(Screen.width - 150, Screen.height - 400, 160, 30), "Move"))
            {
                BoardManager.instance.graph.AllPossiblePath(BoardManager.instance.tiles[TurnManager.instance.CurrentPlayer.X, TurnManager.instance.CurrentPlayer.Z], TurnManager.instance.CurrentPlayer.MoveDistance, TurnManager.instance.CurrentPlayer.JumpDistance);
                SetCurrentState(State.MovePlayer);
                colorPathMove = true;
            }
        }

        if (attack)
        {
            if (GUI.Button(new Rect(Screen.width - 150, Screen.height - 340, 160, 30), "Attack"))
            {
                BoardManager.instance.graph.AllPossiblePathAttack(BoardManager.instance.tiles[TurnManager.instance.CurrentPlayer.X, TurnManager.instance.CurrentPlayer.Z], TurnManager.instance.CurrentPlayer.AttackDistance);
                SetCurrentState(State.AttackPlayer);
            }
        }

        if (endTurn)
        {
            if (GUI.Button(new Rect(Screen.width - 150, Screen.height - 260, 160, 30), "End Turn"))
            {
                if (BoardManager.instance.graph.pathAttack != null)
                {
                    BoardManager.instance.graph.ClearList(BoardManager.instance.graph.pathAttack);
                    BoardManager.instance.graph.pathAttack.Clear();
                }
                TurnManager.instance.ChangePlayer();
                SetCurrentState(State.Idle);
            }
        }

        if (endGame)
        {
            if(GUI.Button(new Rect(Screen.width - 650, Screen.height - 150, 160, 30),"Return Main Menù"))
            {
                SceneManager.LoadScene("Start");
            }
        }
    }
}
