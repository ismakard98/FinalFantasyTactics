﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : Singleton<BoardManager>
{
    public bool create;

    public int width = 15;
    public int height = 15;

    public Tile[,] tiles;
    public Node[,] nodes;
    public Graph graph;
    BoardCreator board;

    public InsertPlayer inPlayer;

    public void Awake()
    {
        tiles = new Tile[width, height];
        graph = new Graph(width, height);
        nodes = graph.CreateGraph(width, height);
        inPlayer = new InsertPlayer();

    }

    void Start () {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                tiles[i, j] = new Tile(i, j, nodes[i, j].height);
            }
        }

        GameObject go = new GameObject();
        go.name = "Board";
        board = go.AddComponent<BoardCreator>();
    }
}
