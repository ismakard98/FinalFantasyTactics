﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TurnManager : Singleton<TurnManager> {

    public bool create;
    public bool infoPossibleAttack;
    public bool loseRed;
    public bool loseBlue;

    public List<Player> players;
    public Queue<Player> turnQueue;
    public Player CurrentPlayer;
    public Player AttackPlayer;

    private int deadPlayerTeam1;
    private int deadPlayerTeam2;

    public void Awake()
    {
        turnQueue = new Queue<Player>();
        players = new List<Player>();
    }

    public void AddPlayerQueue()
    {
        for(int i=0; i<players.Count; i++)
        {
            turnQueue.Enqueue(players[i]);
        }
        CurrentPlayer = turnQueue.Peek();
    }

    public void ChangePlayer()
    {
        turnQueue.Enqueue(CurrentPlayer);
        turnQueue.Dequeue();
        CurrentPlayer = turnQueue.Peek();
    }

    public void CheckDeadPlayer()
    {
        for (int d = 0; d < players.Count; d++)
        {
            if (players[d].HealtPoint <= 0)
            {
                if (players[d].team == 1)
                {
                    players[d].gameObject.SetActive(false);
                    deadPlayerTeam1++;
                }
                else if (players[d].team == 2)
                {
                    players[d].gameObject.SetActive(false);
                    deadPlayerTeam2++;
                }
                BoardManager.instance.tiles[players[d].X, players[d].Z].isOccupied = false;
                BoardManager.instance.tiles[players[d].X, players[d].Z].isSelectable = true;
                players.Remove(players[d]);
                turnQueue.Clear();
                AddPlayerQueue();
            }
        }
    }

    public void Victory()
    {
        if(deadPlayerTeam1 == 3)
        {
            loseBlue = true;
        }
        else if (deadPlayerTeam2 == 3)
        {
            loseRed = true;
        }
    }

    public void UpdateInfoAttack()
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].X == CurrentPlayer.selectAttackX && players[i].Z == CurrentPlayer.selectAttackZ)
            {
                if(CurrentPlayer.team != players[i].team)
                {
                    AttackPlayer = players[i];
                    infoPossibleAttack = true;
                }
                else
                {
                    AttackPlayer = null;
                    infoPossibleAttack = false;
                }
            }
        }
    }
}

