﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherClass : Player {

     public ArcherClass()
     {
        HealtPoint = 7;
        Damage = 3;
        AttackDistance = 5;
        MoveDistance = 5;
        JumpDistance = 2;
        NamePlayer = "Murderess";
     }
}
