﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankClass : Player {

    public TankClass()
    {
        HealtPoint = 5;
        Damage = 4;
        AttackDistance = 3;
        MoveDistance = 3;
        JumpDistance = 2;
        NamePlayer = "Tank";
    }
}
