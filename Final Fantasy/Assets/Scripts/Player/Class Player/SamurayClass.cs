﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SamurayClass : Player
{
    public SamurayClass()
    {
        HealtPoint = 10;
        Damage = 3;
        AttackDistance = 4;
        MoveDistance = 4;
        JumpDistance = 2;
        NamePlayer = "Samuray";
    }
}

