﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private float changeAnimation = 1.5f;
    private int healtPoint;
    private int damage;
    private int attackDistance;
    private int jumpDistance;
    private int moveDistance;
    private string namePlayer;
    Animation animation;

    public Quaternion pos;
    public int X;
    public int Z;
    public int selectedX;
    public int selectedZ;
    public int selectAttackX;
    public int selectAttackZ;
    public int team;
    public int listMove;

    public Vector3 startPosition;
    public Quaternion resetRotation;

    public float currentTime;
    public float maxTime = 0.5f;

    public bool playerMoving;
    public bool playerAttack;

    public int HealtPoint
    {
        get { return healtPoint; }
        set { healtPoint = value; }
    }
    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }
    public int AttackDistance
    {
        get { return attackDistance; }
        set { attackDistance = value; }
    }
    public int JumpDistance
    {
        get { return jumpDistance; }
        set { jumpDistance = value; }
    }
    public int MoveDistance
    {
        get { return moveDistance; }
        set { moveDistance = value; }
    }
    public string NamePlayer
    {
        get { return namePlayer; }
        set { namePlayer = value; }
    }

    public void Start()
    {
        startPosition = transform.position;
        animation = GetComponentInChildren<Animation>();
    }

    public void Update()
    {
        MovePlayer();
        AttackPlayer();
    }

    public void MovePlayer()
    {
        if (playerMoving)
        {
            if (listMove < BoardManager.instance.graph.currentPath.Count)
            {
                if (transform.position != new Vector3(BoardManager.instance.graph.currentPath[listMove].x, BoardManager.instance.graph.currentPath[listMove].height, BoardManager.instance.graph.currentPath[listMove].z))
                {
                    transform.position = Vector3.Lerp(startPosition, new Vector3(BoardManager.instance.graph.currentPath[listMove].x, BoardManager.instance.graph.currentPath[listMove].height, BoardManager.instance.graph.currentPath[listMove].z), currentTime / maxTime);
                    currentTime += Time.deltaTime;
                    RotationPlayer(TurnManager.instance.CurrentPlayer);
                    animation.Play("Run");                   
                }
                else
                {
                    BoardManager.instance.tiles[TurnManager.instance.CurrentPlayer.X, TurnManager.instance.CurrentPlayer.Z].isOccupied = false;
                    TurnManager.instance.CurrentPlayer.X = BoardManager.instance.graph.currentPath[listMove].x;
                    TurnManager.instance.CurrentPlayer.Z = BoardManager.instance.graph.currentPath[listMove].z;
                    BoardManager.instance.tiles[TurnManager.instance.CurrentPlayer.X, TurnManager.instance.CurrentPlayer.Z].isOccupied = true;
                    startPosition = transform.position;
                    currentTime = 0;
                    listMove++;
                }
            }
            else
            {
                GameManager.instance.attack = true;
                GameManager.instance.move = false;
                playerMoving = false;
                listMove = 0;
                animation.Play("Idle");
                if (BoardManager.instance.graph.pathMove != null)
                {
                    BoardManager.instance.graph.ClearList(BoardManager.instance.graph.pathMove);
                    BoardManager.instance.graph.pathMove.Clear();
                }
            }
        }
    }

    public void AttackPlayer()
    {
        if (playerAttack)
        {
            changeAnimation -= Time.deltaTime;
            for (int i = 0; i < TurnManager.instance.players.Count; i++)
            {
                if (TurnManager.instance.players[i].X == TurnManager.instance.CurrentPlayer.selectedX && TurnManager.instance.players[i].Z == TurnManager.instance.CurrentPlayer.selectedZ)
                {
                    if(TurnManager.instance.players[i].team != TurnManager.instance.CurrentPlayer.team)
                    {
                        SetRotationAttack(i);
                        animation.Play("Attack");
                        TurnManager.instance.players[i].animation.Play("Hit");
                        if (changeAnimation <= 0)
                        {
                            animation.Play("Idle");
                            TurnManager.instance.players[i].animation.Play("Idle");
                            if (BoardManager.instance.graph.pathAttack != null)
                            {
                                BoardManager.instance.graph.ClearList(BoardManager.instance.graph.pathAttack);
                                BoardManager.instance.graph.pathAttack.Clear();
                            }
                            TurnManager.instance.players[i].healtPoint = TurnManager.instance.players[i].healtPoint - TurnManager.instance.CurrentPlayer.damage;
                            TurnManager.instance.AttackPlayer = new Player();
                            playerAttack = false;
                        }
                    }
                }
            }
        }

        if (!playerAttack)
        {
            changeAnimation = 2f;
        }
    }

    public void RotationPlayer(Player pl)
    {
        Quaternion rotation;
        int speedRot = 4;

        if (pl.transform.position.x < BoardManager.instance.graph.currentPath[listMove].x)
        {
            rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            pl.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
        }

        if (pl.transform.position.x > BoardManager.instance.graph.currentPath[listMove].x)
        {
            rotation = Quaternion.Euler(new Vector3(0, -90, 0));
            pl.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
        }
        
        if(pl.transform.position.z < BoardManager.instance.graph.currentPath[listMove].z)
        {
            if(pl.transform.rotation.y == -180)
            {
                rotation = Quaternion.Euler(new Vector3(0, -180, 0));
                pl.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
            }
            else 
            {
                rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                pl.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
            }
        }

        if(pl.transform.position.z > BoardManager.instance.graph.currentPath[listMove].z)
        {
            if (pl.transform.rotation.y == -180)
            {
                rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                pl.transform.rotation = Quaternion.Lerp(transform.rotation,rotation, speedRot * Time.deltaTime);
            }
            else 
            {
                rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                pl.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);;
            }
        }
    }

    public void SetRotationAttack(int i)
    {

        Quaternion rotation;
        int speedRot = 4;

        if (TurnManager.instance.CurrentPlayer.X < TurnManager.instance.players[i].X)
        {
            rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            TurnManager.instance.CurrentPlayer.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
        }

        if (TurnManager.instance.CurrentPlayer.X > TurnManager.instance.players[i].X)
        {
            rotation = Quaternion.Euler(new Vector3(0, -90, 0));
            TurnManager.instance.CurrentPlayer.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
        }

        if (TurnManager.instance.CurrentPlayer.Z < TurnManager.instance.players[i].Z)
        {
            if (TurnManager.instance.CurrentPlayer.transform.rotation.y == -180)
            {
                rotation = Quaternion.Euler(new Vector3(0, -180, 0));
                TurnManager.instance.CurrentPlayer.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
            }
            else
            {
                rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                TurnManager.instance.CurrentPlayer.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
            }
        }

        if (TurnManager.instance.CurrentPlayer.Z > TurnManager.instance.players[i].Z)
        {
            if (TurnManager.instance.CurrentPlayer.transform.rotation.y == -180)
            {
                rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                TurnManager.instance.CurrentPlayer.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime);
            }
            else
            {
                rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                TurnManager.instance.CurrentPlayer.transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speedRot * Time.deltaTime); ;
            }
        }
    }
}
