﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsertPlayer : MonoBehaviour
{
    public int playerNumber = 6;
    public bool allPlayerSelected;
    public List<GameObject> player;

    #region InstantiatePlayer
    public void SetPlayer(int x, int z, int height, Quaternion rotation)
    {
        if (playerNumber > 0)
        {
            if (playerNumber == 6)
            {
                GameObject pl = Instantiate(Resources.Load("PlayerPrefab/Samuray") as GameObject, new Vector3(x, height, z), Quaternion.identity);
                pl.AddComponent<SamurayClass>();
                pl.GetComponent<SamurayClass>().X = x;
                pl.GetComponent<SamurayClass>().Z = z;
                pl.transform.rotation = rotation;
                pl.GetComponent<SamurayClass>().team = 1;
                TurnManager.instance.players.Add(pl.GetComponent<SamurayClass>());
                playerNumber--;
                return;
            }
            
            if (playerNumber == 5)
            {
                GameObject pl = Instantiate(Resources.Load("PlayerPrefab/Murderess") as GameObject, new Vector3(x , height, z), Quaternion.identity);
                pl.AddComponent<ArcherClass>();
                pl.GetComponent<ArcherClass>().X = x;
                pl.GetComponent<ArcherClass>().Z = z;
                pl.transform.rotation = rotation;
                pl.GetComponent<ArcherClass>().team = 1;
                TurnManager.instance.players.Add(pl.GetComponent<ArcherClass>());
                playerNumber--;
                return;
            }
            
            if (playerNumber == 4)
            {
                GameObject pl = Instantiate(Resources.Load("PlayerPrefab/Tank") as GameObject, new Vector3(x, height, z), Quaternion.identity);
                pl.AddComponent<TankClass>();
                pl.GetComponent<TankClass>().X = x;
                pl.GetComponent<TankClass>().Z = z;
                pl.transform.rotation = rotation;
                pl.GetComponent<TankClass>().team = 1;
                TurnManager.instance.players.Add(pl.GetComponent<TankClass>());
                playerNumber--;
                return;
            }

            if (playerNumber == 3)
            {
                GameObject pl = Instantiate(Resources.Load("PlayerPrefab/Samuray") as GameObject, new Vector3(x, height, z), Quaternion.identity);
                pl.AddComponent<SamurayClass>();
                pl.GetComponent<SamurayClass>().X = x;
                pl.GetComponent<SamurayClass>().Z = z;
                pl.transform.rotation = rotation;
                pl.GetComponent<SamurayClass>().team = 2;
                TurnManager.instance.players.Add(pl.GetComponent<SamurayClass>());
                playerNumber--;
                return;
            }

            if (playerNumber == 2)
            {
                GameObject pl = Instantiate(Resources.Load("PlayerPrefab/Murderess") as GameObject, new Vector3(x, height, z), Quaternion.identity);
                pl.AddComponent<ArcherClass>();
                pl.GetComponent<ArcherClass>().X = x;
                pl.GetComponent<ArcherClass>().Z = z;
                pl.transform.rotation = rotation;
                pl.GetComponent<ArcherClass>().team = 2;
                TurnManager.instance.players.Add(pl.GetComponent<ArcherClass>());
                playerNumber--;
                return;
            }

            if (playerNumber == 1)
            {
                GameObject pl = Instantiate(Resources.Load("PlayerPrefab/Tank") as GameObject, new Vector3(x, height,z), Quaternion.identity);
                pl.AddComponent<TankClass>();
                pl.GetComponent<TankClass>().X = x;
                pl.GetComponent<TankClass>().Z = z;
                pl.transform.rotation = rotation;
                pl.GetComponent<TankClass>().team = 2;
                TurnManager.instance.players.Add(pl.GetComponent<TankClass>());
                playerNumber--;
                allPlayerSelected = true;
                DestroyThis(playerNumber);
                return;
            }
        }
    }
    #endregion

    public void DestroyThis(int playerNumber)
    {
        if (playerNumber <= 0)
        {
            Destroy(this);
        }
    }
}

