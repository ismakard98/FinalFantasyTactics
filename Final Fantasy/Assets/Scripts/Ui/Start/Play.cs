﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Play : MonoBehaviour {

    public Button play;
    public Button quit;

	void Start () {
        play.onClick.AddListener(Init);
        quit.onClick.AddListener(Quit);
    }
	
    void Init()
    {
        SceneManager.LoadScene("StartGame");        
    }

    void Quit()
    {
        Application.Quit();
    }
}
