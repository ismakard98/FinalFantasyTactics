﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Market : MonoBehaviour {

    public GameObject maincamera;
    public GameObject market;
    public GameObject teamBlue;
    public GameObject teamRed;
    public GameObject infoAttack;

    public Text textBlue;
    public Text textRed;
    public Text enterShop;
    public Text infoRotateCamera;

    private int moneyBlue;
    private int moneyRed;

    private bool team1;
    private bool team2;
    private bool checkMoney;

    private void Awake()
    {
        GameManager.instance.create = true;
    }

    // Update is called once per frame
    void Update()
    {
        MarketInput();
        SetMoneyMarket();
    }


    public void HpSam()
    {
        if (team1)
        {
            if(moneyBlue > 14)
            {
                TurnManager.instance.players[0].HealtPoint = TurnManager.instance.players[0].HealtPoint + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if(moneyRed > 14)
            {
                Debug.Log(1);
                TurnManager.instance.players[3].HealtPoint = TurnManager.instance.players[3].HealtPoint + 1;
                moneyRed = moneyRed - 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
        PlayBuySound();
    }

    public void DamageSam()
    {
        if (team1)
        {
            if (moneyBlue > 14)
            {
                TurnManager.instance.players[0].Damage = TurnManager.instance.players[0].Damage + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if (moneyRed > 14)
            {
                TurnManager.instance.players[3].Damage = TurnManager.instance.players[3].Damage + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        PlayBuySound();
    }

    public void AttackSam()
    {
        if (team1)
        {
            if (moneyBlue > 14)
            {
                TurnManager.instance.players[0].AttackDistance = TurnManager.instance.players[0].AttackDistance + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if (moneyRed > 14)
            {
                TurnManager.instance.players[3].AttackDistance = TurnManager.instance.players[3].AttackDistance + 1;
                moneyRed = moneyRed - 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
        PlayBuySound();
    }



    public void JumpMurd()
    {
        if (team1)
        {
            if (moneyBlue > 14)
            {
                TurnManager.instance.players[1].JumpDistance = TurnManager.instance.players[1].JumpDistance + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if (moneyRed > 14)
            {
                TurnManager.instance.players[4].JumpDistance = TurnManager.instance.players[4].JumpDistance + 1;
                moneyRed = moneyRed - 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
        PlayBuySound();
    }

    public void MoveMurd()
    {
        if (team1)
        {
            if (moneyBlue > 14)
            {
                TurnManager.instance.players[1].MoveDistance = TurnManager.instance.players[1].MoveDistance + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if (moneyRed > 14)
            {
                TurnManager.instance.players[4].MoveDistance = TurnManager.instance.players[4].MoveDistance + 1;
                moneyRed = moneyRed - 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
        PlayBuySound();
    }

    public void AttackMurd()
    {
        if (team1)
        {
            if (moneyBlue > 14)
            {
                TurnManager.instance.players[1].AttackDistance = TurnManager.instance.players[1].AttackDistance + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if (moneyRed > 14)
            {
                TurnManager.instance.players[4].AttackDistance = TurnManager.instance.players[4].AttackDistance + 1;
                moneyRed = moneyRed - 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
        PlayBuySound();
    }



    public void JumpTank()
    {
        if (team1)
        {
            if (moneyBlue > 14)
            {
                TurnManager.instance.players[2].JumpDistance = TurnManager.instance.players[2].JumpDistance + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if (moneyRed > 14)
            {
                TurnManager.instance.players[5].JumpDistance = TurnManager.instance.players[5].JumpDistance + 1;
                moneyRed = moneyRed - 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
        PlayBuySound();
    }

    public void DamageTank()
    {
        if (team1)
        {
            if (moneyBlue > 14)
            {
                TurnManager.instance.players[2].Damage = TurnManager.instance.players[2].Damage + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if (moneyRed > 14)
            {
                TurnManager.instance.players[5].Damage = TurnManager.instance.players[5].Damage + 1;
                moneyRed = moneyRed - 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
        PlayBuySound();
    }

    public void AttackTank()
    {
        if (team1)
        {
            if (moneyBlue > 14)
            {
                TurnManager.instance.players[2].AttackDistance = TurnManager.instance.players[2].AttackDistance + 1;
                moneyBlue = moneyBlue - 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (team2)
        {
            if (moneyRed > 14)
            {
                TurnManager.instance.players[5].AttackDistance = TurnManager.instance.players[5].AttackDistance + 1;
                moneyRed = moneyRed - 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
        PlayBuySound();
    }



    public void MarketInput()
    {
        if (Input.GetKey(KeyCode.M))
        {
            if (GameManager.instance.AllPlayerPositioned)
            {
                if (TurnManager.instance.CurrentPlayer.team == 1)
                {
                    team1 = true;
                    teamBlue.SetActive(true);
                    team2 = false;
                    teamRed.SetActive(false);
                }
                else if (TurnManager.instance.CurrentPlayer.team == 2)
                {
                    team2 = true;
                    teamRed.SetActive(true);
                    team1 = false;
                    teamBlue.SetActive(false);
                }
                enterShop.text = " ";
                market.SetActive(true);
                maincamera.SetActive(false);
                infoRotateCamera.text = " ";
                infoAttack.SetActive(false);
                SoundManager.instance.Play(SoundManager.instance.openShopSound, false);
            }
        }
        else if (Input.GetKey(KeyCode.N))
        {
            if (GameManager.instance.AllPlayerPositioned)
            {
                enterShop.text = "For enter in the Market press 'M' ";
                infoRotateCamera.text = "For move camera press ' < ' or '>' ";
                market.SetActive(false);
                maincamera.SetActive(true);
                SoundManager.instance.Play(SoundManager.instance.closeShopSound, false);
            }
        }
    }

    public void SetMoneyMarket()
    {
        if (teamBlue)
        {
            if (Input.GetKeyDown(KeyCode.B))
            {
                moneyBlue = moneyBlue + 15;
                textBlue.text = "Money Blue:" + moneyBlue;
            }
        }
        else if (teamRed)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                moneyRed = moneyRed + 15;
                textRed.text = "Money Red:" + moneyRed;
            }
        }
    }

    private void PlayBuySound()
    {
        SoundManager.instance.Play(SoundManager.instance.soundBuy, false);
    }
}
