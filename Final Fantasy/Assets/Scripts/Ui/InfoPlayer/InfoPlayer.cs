﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPlayer : MonoBehaviour {

    public GameObject ChoosePositionPlayer;
    public GameObject UiGamePlayer;

    public Text healtPoint;
    public Text damage;
    public Text classPlayer;
    public Text team;
    public Text positionPlayer;

    public GameObject infoAttack;
    public Text healtPointAtt;
    public Text damageAtt;
    public Text classPlayerAtt;
    public Text teamAtt;

    public GameObject finishGame;
    public Text winTeam;

    void Update () {
        if (!GameManager.instance.AllPlayerPositioned)
        {
            ChoosePositionPlayer.SetActive(true);
            positionPlayer.text = "Choose the position of the players";
        }

        if (GameManager.instance.AllPlayerPositioned)
        {
            UiGamePlayer.SetActive(true);
            ChoosePositionPlayer.SetActive(true);
            positionPlayer.text = "";
            if (TurnManager.instance.infoPossibleAttack)
            {
                UpdateInfoAttack();
            }
            else if (!TurnManager.instance.infoPossibleAttack)
            {
                infoAttack.SetActive(false);
            }
            UpdateInfoPlayer();
        }

        if(TurnManager.instance.loseBlue)
        {
            GameManager.instance.endGame = true;
            winTeam.text = "Win Team Red!!!! Congratulations";
            finishGame.SetActive(true);
        }

        else if (TurnManager.instance.loseRed)
        {
            GameManager.instance.endGame = true;
            winTeam.text = "Win Team Blue!!!! Congratulations";
            finishGame.SetActive(true);
        }
	}

    public void UpdateInfoPlayer()
    {
        healtPoint.text = "Healt Point = " + TurnManager.instance.CurrentPlayer.HealtPoint;
        damage.text = "Damage = " + TurnManager.instance.CurrentPlayer.Damage;
        team.text = "Team = " + TurnManager.instance.CurrentPlayer.team;

        if(TurnManager.instance.CurrentPlayer.team == 1)
        {
            classPlayer.text = "Class " + "Blue = " + TurnManager.instance.CurrentPlayer.NamePlayer;
            classPlayer.color = Color.blue;
            healtPoint.color = Color.blue;
            damage.color = Color.blue;
            team.color = Color.blue;
        }

        else
        {
            classPlayer.text = "Class " + "Red = " + TurnManager.instance.CurrentPlayer.NamePlayer;
            classPlayer.color = Color.red;
            healtPoint.color = Color.red;
            damage.color = Color.red;
            team.color = Color.red;
        }

    }

    public void UpdateInfoAttack()
    {
        infoAttack.SetActive(true);
        healtPointAtt.text = "Healt Point = " + TurnManager.instance.AttackPlayer.HealtPoint;
        damageAtt.text = "Damage = " + TurnManager.instance.AttackPlayer.Damage;
        teamAtt.text = "Team = " + TurnManager.instance.AttackPlayer.team;

        if (TurnManager.instance.AttackPlayer.team == 1)
        {
            classPlayerAtt.text = "Class " + "Blue = " + TurnManager.instance.AttackPlayer.NamePlayer;
            classPlayerAtt.color = Color.blue;
            healtPointAtt.color = Color.blue;
            damageAtt.color = Color.blue;
            teamAtt.color = Color.blue;
        }

        else
        {
            classPlayerAtt.text = "Class " + "Red = " + TurnManager.instance.AttackPlayer.NamePlayer;
            classPlayerAtt.color = Color.red;
            healtPointAtt.color = Color.red;
            damageAtt.color = Color.red;
            teamAtt.color = Color.red;
        }
    }
}
